# stunning-signature

**本项目是基于开源项目[stunning-signature]进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/DimaKoz/stunning-signature）追踪到原项目版本

#### 项目介绍

- 项目名称：获得安装包的Native层签名
- 所属系列：ohos的第三方组件适配移植
- 功能：从Navtive层取得包的签名
- 项目移植状态：完成
- 调用差异：无
- 原项目Doc地址：https://github.com/DimaKoz/stunning-signature
- 原项目基线版本：v1.01 b6656d39467a4c285f060411559bdd9518977b86
- 编程语言：C
- 外部库依赖：无

#### 展示效果

![](./pic/image.png)

#### 使用说明

1、直接安装包文件，运行应用

2、取得Native层签名的lib接口使用方法，可以参考下面路径的sample code。

 stunning-signature/entry/src/main/java/com/kozhevin/signverification/slice

过程：

①'native-lib' native library封装了JNI接口Java_com_kozhevin_signverification_slice_MainAbilitySlice_bytesFromJNI(JNIEnv *env, jobject this) 

②应用程序通过JNI接口bytesFromJNI()接口取得签名信息。

③应用程序再通过String getInfoFromBytes(byte[] bytes)接口，解析签名信息，输出到应用界面上。      

#### 版本迭代

- V1.0.2

  C代码移植到ohos平台

- v1.0.1

  Features Obtain apk's signature on the native layer.

#### 版权和许可信息

- MIT License

