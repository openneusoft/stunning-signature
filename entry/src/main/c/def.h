

#ifndef NATIVESIGNATUREVERIFICATION_DEF_H
#define NATIVESIGNATUREVERIFICATION_DEF_H

#include <jni.h>
#include <stdio.h>

#define NSV_LOG_TAG "SignVerification"


#ifndef NDEBUG

#define NSV_LOGI(...)  printf(__VA_ARGS__)
#define NSV_LOGE(...)  printf(__VA_ARGS__)
#define NSV_LOGW(...)  printf(__VA_ARGS__)
#define NSV_LOGD(...)  printf(__VA_ARGS__)

#else //NDEBUG

#define NSV_LOGI(...)
#define NSV_LOGE(...)
#define NSV_LOGW(...)
#define NSV_LOGD(...)
#define NSV_LOGV(...)

#endif //NDEBUG

#endif //NATIVESIGNATUREVERIFICATION_DEF_H
