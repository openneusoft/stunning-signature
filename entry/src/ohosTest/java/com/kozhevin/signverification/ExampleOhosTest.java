package com.kozhevin.signverification;

import com.kozhevin.signverification.slice.MainAbilitySlice;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Method;

public class ExampleOhosTest {

    @Test
    public void bytesFromJNI() throws Exception {
        MainAbilitySlice mainAbilitySlice = new MainAbilitySlice();
        Method method = MainAbilitySlice.class.getDeclaredMethod("bytesFromJNI");
        method.setAccessible(true);
        byte[] actualRes = (byte[]) method.invoke(mainAbilitySlice);
        Assert.assertNotNull(actualRes);
        Assert.assertTrue(actualRes.length > 0);
    }

    @Test
    public void getInfoFromBytes() throws Exception {
        MainAbilitySlice mainAbilitySlice = new MainAbilitySlice();
        Method bytesFromJNI = MainAbilitySlice.class.getDeclaredMethod("bytesFromJNI");
        Method getInfoFromBytes = MainAbilitySlice.class.getDeclaredMethod("getInfoFromBytes", byte[].class);
        bytesFromJNI.setAccessible(true);
        getInfoFromBytes.setAccessible(true);
        byte[] rawCertNative = (byte[]) bytesFromJNI.invoke(mainAbilitySlice);
        Assert.assertNotNull(rawCertNative);
        Assert.assertTrue(rawCertNative.length > 0);
    }

    @Test
    public void getInfoFromBytesByNull() throws Exception {
        MainAbilitySlice mainAbilitySlice = new MainAbilitySlice();
        Method getInfoFromBytes = MainAbilitySlice.class.getDeclaredMethod("getInfoFromBytes", byte[].class);
        getInfoFromBytes.setAccessible(true);
        byte[] bytes = null;
        String actualRes = (String) getInfoFromBytes.invoke(mainAbilitySlice, bytes);
        String expectedRes = "null";
        Assert.assertEquals(expectedRes, actualRes);
    }

}